<?php

use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();


Route::group(['prefix' => 'admin','middleware' => 'is_admin'], function() {
    Route::get('home', [HomeController::class, 'adminHome'])->name('admin.home');
});

Route::group(['prefix' => 'developer','middleware' => 'is_developer'], function() {
    Route::get('home', [HomeController::class, 'developerHome'])->name('developer.home');
});

Route::group(['prefix' => 'user','middleware' => 'is_user'], function() {
    Route::get('home', [HomeController::class, 'userHome'])->name('user.home');
});


Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
