<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class CreateUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [

            [
               'name'=>'Admin',
               'email'=>'admin@gmail.com',
                'type '=>'1',
                'role '=>'1',
                'mobile' => '123456789',
                'address' => 'Feni',
               'password'=> bcrypt('123456'),

            ],

            [

               'name'=>'User',
               'email'=>'user@gmail.com',
                'type '=>'3',
                'mobile' => '123456789',
                'address' => 'Feni',
               'password'=> bcrypt('123456'),

            ],

        ];

  

        foreach ($user as $key => $value) {

            User::create($value);

        }
    }
}
