<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class IsUser
{
    
    public function handle(Request $request, Closure $next)
    {
        if(auth()->user()->type == 3){
            return $next($request);
        }

        return redirect('home')->with('error',"You don't have user access.");
    }
}
